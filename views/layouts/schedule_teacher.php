<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\RaspAsset;
use app\helpers\ThisDay;

RaspAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://scriptjava.net/source/scriptjava/scriptjava.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php include 'menu.php'; ?>

<div class="l-main">
    <div class="uk-container uk-container-center b-main">
        <label>Преподаватель </label>
        <input id="teacher" type="text" name="teacher" list="teacher_list" placeholder="Преподаватель" style="width:250px;" value="<?php
        if (isset($_GET['teacher']))
            echo $_GET['teacher'];
        elseif (isset($_COOKIE['teacher']))
            echo $_COOKIE['teacher'];
        else
            echo "Федоров Алексей Роальдович";
        ?>"/>
        <datalist id="teacher_list">
            <?php
            foreach (ThisDay::getTeachersName() as $val) { ?>
                <option value = "<?= $val['fullName'] ?>" ><?= $val['fullName'] ?></option >
            <?php }
            ?>
        </datalist>
        <?= $content ?>

    </div><!-- .uk-container.b-main -->
</div><!-- .l-main -->

<!-- BEGIN FOOT -->
<div class="l-foot">
    <div class="l-foot-inner">
        <div class="b-foot">

        </div><!-- .b-foot -->
    </div><!-- .l-foot-inner -->
</div><!-- .l-foot -->
</div><!-- .l-wrap -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
