<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\RaspAsset;
use app\helpers\ThisDay;

RaspAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://scriptjava.net/source/scriptjava/scriptjava.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="note" class="uk-modal">

    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">
            <h2>Заметки по предмему "<span id="schName"></span>"</h2>
        </div>
        <div id="notes_list" class="b-list b-list_notes">
        </div><!-- .b-list.b-list_notes -->
        <div class="b-addNote">
            <form class="uk-form" method="post" action="/index.php?r=notes/create">
                <input type="hidden" id="disciplineId" name="disciplineId" value=""/>
                <h3>Добавить новую заметку:</h3>
                <div class="uk-form-row">
                    <textarea name="text"></textarea>
                    <button class="uk-button uk-button-success" type="submit">Добавить</button>
                </div>
            </form>
        </div>
    </div>

</div><!-- .uk-modal -->

<?php include 'menu.php'; ?>

    <div class="l-main">
        <div class="uk-container uk-container-center b-main">
            <label>Группа </label>
            <input id="group" type="text" name="group" list="group_list" placeholder="Группа" value="<?php
            if (isset($_GET['group']))
                echo $_GET['group'];
            elseif (!Yii::$app->getUser()->isGuest && Yii::$app->getUser()->getIdentity()->student)
                echo Yii::$app->getUser()->getIdentity()->group;
            elseif (isset($_COOKIE['group']))
                echo $_COOKIE['group'];
            else
                echo "МП-34";
            ?>"/>
            <datalist id="group_list">
                <?php 
                foreach (ThisDay::getGroupsName() as $val) { ?>
                    <option value = "<?= $val['name'] ?>" ><?= $val['name'] ?></option >
                <?php }
                ?>
            </datalist>
            <div class="b-changeMode">
                <div class="uk-button-group">
                    <a class="uk-button uk-button-small uk-button-primary <?= (isset($_GET['type']) && $_GET['type'] == 'day') || !isset($_GET['type']) ? 'uk-active' : ''; ?>"
                       href="index.php?r=rasp/show-rasp&type=day">День</a>
                    <a class="uk-button uk-button-small uk-button-primary <?= (isset($_GET['type']) && $_GET['type'] == 'week') ? 'uk-active' : ''; ?>"
                       href="index.php?r=rasp/show-rasp&type=week">Неделя</a>
                    <a class="uk-button uk-button-small uk-button-primary <?= (isset($_GET['type']) && $_GET['type'] == 'month') ? 'uk-active' : ''; ?>"
                       href="index.php?r=rasp/show-rasp&type=month">Месяц</a>
                </div>
            </div><!-- .b-changeMode -->
            <?= $content ?>

        </div><!-- .uk-container.b-main -->
    </div><!-- .l-main -->

    <!-- BEGIN FOOT -->
    <div class="l-foot">
        <div class="l-foot-inner">
            <div class="b-foot">

            </div><!-- .b-foot -->
        </div><!-- .l-foot-inner -->
    </div><!-- .l-foot -->
</div><!-- .l-wrap -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
