<?php use app\helpers\ThisDay; ?>
<div id="reg" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Регистрация</div>
        <div class="uk-alert">
Регистрация позволит студентам оставлять заметки по предметам и отмечать посещение занятий. Преподаатели
            получат возможность отправлять сообщения студенческим группам.
        </div>
        <form class="uk-form uk-form-width-medium uk-container-center" id="regForm" method="post"
              action="/index.php?r=auth/registr">
            <div class="uk-form-row">
                <label class="uk-form-label">ФИО</label>
                <div class="uk-form-controls">
                    <input type="text" name="name" placeholder="">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Email</label>
                <div class="uk-form-controls">
                    <input type="email" name="email" placeholder="">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Пароль</label>
                <div class="uk-form-controls">
                    <input type="password" name="password" placeholder="">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Кто вы?</label>
                <div class="uk-form-controls">
                    <label>
                        <input type="radio" name="student" value="1" checked class="i-personType"> Студент
                    </label>
                    <label>
                        <input type="radio" name="student" value="0" class="i-personType"> Преподаватель
                    </label>
                </div>
            </div>
            <div class="uk-form-row" id="fieldForStudent">
                <label class="uk-form-label">Группа</label>
                <div class="uk-form-controls">
                    <input type="text" name="group" list="group_list" placeholder="Группа" value="МП-34"/>
                </div>
            </div>
            <div class="uk-form-row b-hidden" id="fieldForPrepod">
                <label class="uk-form-label">Ключ</label>
                <div class="uk-form-controls">
                    <input type="text" name="key" placeholder="">
                </div>
            </div>
            <div class="uk-form-row">
                <button type="submit" class="uk-button uk-button-large uk-button-primary">Зарегистрироваться</button>
            </div>
            <script>
$("#regForm .i-personType").change(function () {
    if ($(this).val() == "0") {
        $("#fieldForStudent").hide(200);
        $("#fieldForPrepod").show(200);
    } else {
        $("#fieldForStudent").show(200);
        $("#fieldForPrepod").hide(200);
    }
});
            </script>
        </form>
    </div>
</div><!-- .uk-modal -->
<div id="login" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">Войти</div>
        <form class="uk-form uk-form-width-medium uk-container-center" id="loginForm" method="post"
              action="/index.php?r=auth/login">
            <div class="uk-form-row">
                <label class="uk-form-label">Email</label>
                <div class="uk-form-controls">
                    <input type="email" name="email" placeholder="">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Пароль</label>
                <div class="uk-form-controls">
                    <input type="password" name="password" placeholder="">
                </div>
            </div>
            <div class="uk-form-row">
                <button type="submit" class="uk-button uk-button-large uk-button-primary">Войти</button>
            </div>
        </form>
    </div>
</div><!-- .uk-modal -->

<div class="l-wrap">
    <div class="l-head">
        <div class="l-head-inner">
            <div class="uk-container uk-container-center">
                <div class="b-head">
                    <nav class="uk-navbar">
                        <a class="uk-navbar-brand uk-hidden-small" href="/index.php">Расписание МИЭТ</a>
                        <a class="uk-navbar-brand uk-hidden-small" href="/index.php?type=teacher">Расписание преподаватели</a>
                        <ul class="uk-navbar-nav">
                            <?php if (Yii::$app->user->isGuest) { ?>
    <li>
        <a href="#login" data-uk-modal>
            Войти
        </a>
    </li>
    <li>
        <a href="#reg" data-uk-modal>
            Зарегистрироваться
        </a>
    </li>
<?php } else { ?>
    <li>
        <a href="/index.php?r=auth/logout">
            <?php echo Yii::$app->session->get('userName'); ?> (Выйти)
        </a>
    </li>
<?php }

                            if (!Yii::$app->getUser()->isGuest && !Yii::$app->getUser()->getIdentity()->student) {
                                ?>
                                <li>
                                    <a href="/index.php?r=teacher" class="">
                                        Отправка сообщений
                                    </a>
                                </li>
                            <?php }elseif(!Yii::$app->getUser()->isGuest){ ?>
                                <li>
                                    <a href="/index.php?r=teacher/user-message" class="">
                                        Сообщения <div id="countMesseges" class="uk-badge uk-badge-notification" style="display:none;">За день </div>
                                        <input id="isUser" type="hidden" value="<?=!Yii::$app->getUser()->isGuest?>" />
                                        <input id="isStudent" type="hidden" value="<?=!Yii::$app->getUser()->getIdentity()->student?>" />
                                    </a>
                                </li>
                            <?php }?>


<li>
    <?php if ($err = Yii::$app->session->getFlash('err', null, true)) {
        echo 'Ошибка: ' . $err[0];
    } ?>
</li>
</ul>
<div class="uk-navbar-flip">
    <div class="uk-navbar-content">
        <?php $thisWeek = ThisDay::getThisWeek(); ?>
        <span class="uk-badge">Неделя <?= $thisWeek['week'] ?> (<?= $thisWeek['type'] ?>)</span>
    </div>
</div>
</nav><!-- .uk-navbar -->
</div><!-- .b-head -->
</div><!-- .uk-container -->
</div><!-- .l-head-inner -->
</div><!-- .l-head -->
