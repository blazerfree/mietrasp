<div class="l-main">
	<div class="uk-container uk-container-center b-main">
		<!-- END HEAD -->
		<!-- BEGIN CONTENT -->
		<h1>Входящие сообщения</h1>
		<div class="b-list b-list_notes">
			<?php
			if(empty($messages) || count($messages)==0)
				echo "Сообщений нет";
			else{
			foreach ($messages as $message){ ?>
    		<div class="b-note uk-panel uk-panel-box uk-panel-box-secondary">
			    <div class="b-note-date uk-badge uk-badge-info">
			    	<?= date('j.n.Y',$message['created_at']) ?>
			    </div>
			    <div class="b-note-author uk-badge uk-badge-warning">
					<?=$message['teacher']['name']?>
			    </div>
				<div class="b-note-text">
					<?=$message['text']?>
				</div>
    		</div><!-- .b-note -->

			<?php }}?>
    	</div>
		<!-- END CONTENT -->
		<!-- BEGIN FOOT -->
	</div><!-- .uk-container.b-main -->
</div><!-- .l-main -->
