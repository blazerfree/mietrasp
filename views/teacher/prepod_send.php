<div class="col-md-5 col-sm-offset-3 l-main">
    <div class="uk-container uk-container-center b-main">
        <!-- END HEAD -->
        <!-- BEGIN CONTENT -->
        <h1>Отправить сообщение группам</h1>
        <div class="b-msgSendForm">
            <form class="uk-form" method="post" action="/index.php?r=teacher/send-message">
                <div class="">
                    <div class="prepod-line">
                        <legend style="margin-bottom:10px;padding:5px;">Форма отправки сообщений</legend>
                        <div class="b-selectGroups">
                            <div class="prepod-line">
                                <label for="">Выберите группы, которым хотите отправить сообщение</label>
                            </div>
                            <div class="prepod-line">
                                <input id="group" type="text" name="group" list="prepod_group_list" placeholder="Группа"
                                       value="<?php
                                       if (isset($_GET['group']))
                                           echo $_GET['group'];
                                       elseif (isset($_COOKIE['group']))
                                           echo $_COOKIE['group'];
                                       else
                                           echo "МП-34";
                                       ?>"/>
                                <datalist id="prepod_group_list">
                                    <?php for ($i = 1; $i < 5; $i++) {
                                        for ($j = 0; $j < 10; $j++) {
                                            echo "<option value = 'МП-$i$j' >МП-$i$j</option >";
                                        }
                                    }
                                    ?>
                                </datalist>
                            </div>
                        </div>
                        <div class="prepod-line">
                            <label for="">Введите сообщение:</label>
                        </div>
                        <div class="prepod-line">
                            <textarea rows="6" class="prepod-line" name="text" placeholder="Текст сообщения"></textarea>
                        </div>
                        <div class="prepod-line">
                            <button class="uk-button uk-button-primary uk-width-1-1">Отправить</button>
                        </div>
                        <!-- <button class="uk-button uk-width-1-1 uk-button-success">Успешно!</button> -->
                        <!-- <button class="uk-button uk-button-primary" type="reset">Отправить еще одно</button> -->
                    </div>
                </div>
            </form>
        </div><!-- .b-msgSendForm> -->
        <!-- END CONTENT -->
        <!-- BEGIN FOOT -->
    </div><!-- .uk-container.b-main -->
</div><!-- .l-main -->
