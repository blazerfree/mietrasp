<?php
use app\helpers\ThisDay;

$thisWeek = ThisDay::getThisWeek()['week'] % 4;
?>
<div class="uk-grid">
    <div class="uk-width-large-1-1">
        <table id="table_subjects" class="b-table b-table_subjects uk-table">
            <colgroup></colgroup>
            <colgroup class="<?= $thisWeek == 1 ? 'm-active' : '' ?>"></colgroup>
            <colgroup class="<?= $thisWeek == 2 ? 'm-active' : '' ?>"></colgroup>
            <colgroup class="<?= $thisWeek == 3 ? 'm-active' : '' ?>"></colgroup>
            <colgroup class="<?= $thisWeek == 0 ? 'm-active' : '' ?>"></colgroup>

            <tbody class="b-table-head">
            <tr>
                <td></td>
                <td>1 числитель</td>
                <td>1 знаменатель</td>
                <td>2 числитель</td>
                <td>2 знаменатель</td>
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tbody><!-- .b-table-head -->
            <?php foreach ($subjects as $i => $subject) { ?>
                <tbody class="b-subject">
                <tr class="b-subject-row i-subject-row" data-subject-day="1">
                    <td rowspan="6" class="b-subject-name"><?= $subject['discipline']['name'] ?></td>
                    <!-- первая неделя -->
                    <td id="<?= $id = (($i + 1) . '11') ?>" class="b-subject-week i-subject-week">
                        <?php
                        $flag = false;
                        if (isset($subject['лек'])) {
                            if (isset($subject['лек'][1])) {
                                if (isset($subject['лек'][1][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['пр'])) {
                            if (isset($subject['пр'][1])) {
                                if (isset($subject['пр'][1][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								    </span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['лаб'])) {
                            if (isset($subject['лаб'][1])) {
                                if (isset($subject['лаб'][1][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['days'])) {
                            if (isset($subject['days'][1])) {
                                if (isset($subject['days'][1]) && key_exists(1, $subject['days'][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (!$flag)
                            echo '<script>setEmptyTd(' . ($id) . ')</script>';
                        ?>
                    </td>
                    <!-- вторая неделя -->
                    <td id="<?= $id = (($i + 1) . '21') ?>" class="b-subject-week i-subject-week">
                        <?php
                        $flag = false;
                        if (isset($subject['лек'])) {
                            if (isset($subject['лек'][2])) {
                                if (isset($subject['лек'][2][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['пр'])) {
                            if (isset($subject['пр'][2])) {
                                if (isset($subject['пр'][2][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								    </span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['лаб'])) {
                            if (isset($subject['лаб'][2])) {
                                if (isset($subject['лаб'][2][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['days'])) {
                            if (isset($subject['days'][2])) {
                                if (isset($subject['days'][2]) && key_exists(1, $subject['days'][2])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (!$flag)
                            echo '<script>setEmptyTd(' . ($id) . ')</script>';
                        ?>
                    </td>
                    <!-- третья неделя -->
                    <td id="<?= $id = (($i + 1) . '31') ?>" class="b-subject-week i-subject-week">
                        <?php
                        $flag = false;
                        if (isset($subject['лек'])) {
                            if (isset($subject['лек'][3])) {
                                if (isset($subject['лек'][3][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['пр'])) {
                            if (isset($subject['пр'][3])) {
                                if (isset($subject['пр'][3][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								    </span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['лаб'])) {
                            if (isset($subject['лаб'][3])) {
                                if (isset($subject['лаб'][3][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['days'])) {
                            if (isset($subject['days'][3])) {
                                if (isset($subject['days'][3]) && key_exists(1, $subject['days'][3])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (!$flag)
                            echo '<script>setEmptyTd(' . ($id) . ')</script>';
                        ?>
                    </td>
                    <!-- четвертая неделя -->
                    <td id="<?= $id = (($i + 1) . '41') ?>" class="b-subject-week i-subject-week">
                        <?php
                        $flag = false;
                        if (isset($subject['лек'])) {
                            if (isset($subject['лек'][4])) {
                                if (isset($subject['лек'][4][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['пр'])) {
                            if (isset($subject['пр'][4])) {
                                if (isset($subject['пр'][4][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								    </span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['лаб'])) {
                            if (isset($subject['лаб'][4])) {
                                if (isset($subject['лаб'][4][1])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (isset($subject['days'])) {
                            if (isset($subject['days'][4])) {
                                if (isset($subject['days'][4]) && key_exists(1, $subject['days'][4])) {
                                    echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>';
                                    $flag = true;
                                }
                            }
                        }
                        if (!$flag)
                            echo '<script>setEmptyTd(' . ($id) . ')</script>';
                        ?>
                    </td>
                </tr>

                <?php
                for ($d = 2; $d < 7; $d++) {
                    ?>
                    <tr class="b-subject-row i-subject-row" data-subject-day="<?= $d ?>">
                        <?php for ($w = 1; $w < 5; $w++) { ?>

                            <td id="<?= $id = (($i + 1) . '' . $w . '' . $d) ?>" class="b-subject-week i-subject-week">
                                <?php
                                $flag = false;
                                if (isset($subject['лек'])) {
                                    if (isset($subject['лек'][$w])) {
                                        if (isset($subject['лек'][$w][$d])) {
                                            echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>';
                                            $flag = true;
                                        }
                                    }
                                }
                                if (isset($subject['пр'])) {
                                    if (isset($subject['пр'][$w])) {
                                        if (isset($subject['пр'][$w][$d])) {
                                            echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								    </span>';
                                            $flag = true;
                                        }
                                    }
                                }
                                if (isset($subject['лаб'])) {
                                    if (isset($subject['лаб'][$w])) {
                                        if (isset($subject['лаб'][$w][$d])) {
                                            echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>';
                                            $flag = true;
                                        }
                                    }
                                }
                                if (isset($subject['days'])) {
                                    if (isset($subject['days'][$w])) {
                                        if (isset($subject['days'][$w]) && key_exists($d, $subject['days'][$w])) {
                                            echo '<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>';
                                            $flag = true;
                                        }
                                    }
                                }
                                if (!$flag)
                                    echo '<script>setEmptyTd(' . ($id) . ')</script>';
                                ?>
                            </td>

                        <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            <?php } ?>
        </table><!-- .b-subject -->

        <div class="find-teacher">
            <p>Где преподватель?</p>
            <div>
                <?php $thisWeek = ThisDay::getThisWeek(); ?>
                <table class="find">
                    <tbody class="find">
                    <tr>
                        <td><p>Неделя</p></td>
                        <td><p>День</p></td>
                        <td><p>Номер пары </p></td>
                    </tr>

                    <tr>
                        <td>
                            <input id="findWeek" type="text"
                                   value="<?= !isset($_GET['fweek']) ? $thisWeek['week'] : $_GET['fweek']; ?>"
                                   placeholder="Неделя"/>
                        </td>
                        <td>
                            <select id="findDay" name="findDay">
                                <option name="findDay"
                                        value="1" <?php if(isset($_GET['fday']) && $_GET['fday'] == 1) echo "selected"; else if(date('N')==1) echo "selected"; ?>>
                                    Понедельник
                                </option>
                                <option name="findDay"
                                        value="2" <?php if(isset($_GET['fday']) && $_GET['fday'] == 2) echo "selected"; else if(date('N')==2) echo "selected"; ?>>
                                    Вторник
                                </option>
                                <option name="findDay"
                                        value="3" <?php if(isset($_GET['fday']) && $_GET['fday'] == 3) echo "selected"; else if(date('N')==3) echo "selected"; ?>>
                                    Среда
                                </option>
                                <option name="findDay"
                                        value="4" <?php if(isset($_GET['fday']) && $_GET['fday'] == 4) echo "selected"; else if(date('N')==4) echo "selected"; ?>>
                                    Четверг
                                </option>
                                <option name="findDay"
                                        value="5" <?php if(isset($_GET['fday']) && $_GET['fday'] == 5) echo "selected"; else if(date('N')==5) echo "selected"; ?>>
                                    Пятница
                                </option>
                                <option name="findDay"
                                        value="6" <?php if(isset($_GET['fday']) && $_GET['fday'] == 6) echo "selected"; else if(date('N')==6) echo "selected"; ?>>
                                    Суббота
                                </option>
                            </select>
                        </td>
                        <td>
                            <input id="findP" type="text" value="<?= isset($_GET['fp']) ? $_GET['fp'] : 1; ?>" placeholder="Номер пары"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <button onclick="findTeacher()">Найти</button>
                <div>
                    <p>Предположительное местонахождение: <label id="whereTeacher"><?= is_null($whereTeacher) ? "Не известно" : $whereTeacher->auditory ?> </label></p>
                </div>
            </div>
        </div>


        <script>
            var $table_subjects = $("#table_subjects");
            var subjectRow = $table_subjects.find(".i-subject-row");
            var colgroup = $table_subjects.find("colgroup");

            subjectRow.each(function () {
                var $el = $(this);
                var day = $el.data("subject-day");
                var day_name;
                var weeks = $el.find(".i-subject-week");

                switch (day) {
                    case 1:
                        day_name = "пн";
                        break;
                    case 2:
                        day_name = "вт";
                        break;
                    case 3:
                        day_name = "ср";
                        break;
                    case 4:
                        day_name = "чт";
                        break;
                    case 5:
                        day_name = "пт";
                        break;
                    case 6:
                        day_name = "сб";
                        break;
                    case 7:
                        day_name = "вс";
                        break;
                    default:
                        day_name = "er"
                }

                weeks.append("<span class='b-subject-day'>" + day_name + "</span>");
            });

            $table_subjects.delegate('td', 'mouseover mouseleave', function (e) {
                var $cell = $(this);
                if (e.type == 'mouseover') {
                    $cell.parent().addClass("m-active");
                    colgroup.eq($cell.index()).addClass("m-hover");
                } else {
                    $cell.parent().removeClass("m-active");
                    colgroup.eq($cell.index()).removeClass("m-hover");
                }
            });
        </script>
    </div><!-- .uk-width-large-1-1 -->
</div><!-- .uk-grid -->
<!-- END CONTENT -->