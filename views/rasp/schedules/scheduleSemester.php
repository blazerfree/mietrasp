
<div id="note" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-header">
            <h2>Заметки по предмему "<название>"</h2>
        </div>
        <div class="b-list b-list_notes">
            <div class="b-note uk-panel uk-panel-box uk-panel-box-secondary">
                <div class="b-note-delete">
                    <a href="#" class="uk-button uk-button-mini uk-button-danger"><i class="uk-icon-close"></i></a>
                </div>
                <div class="b-note-date uk-badge uk-badge-info">
                    10.10.2016
                </div>
                <div class="b-note-text">
                    Текст заметки
                </div>
            </div><!-- .b-note -->
            <div class="b-note uk-panel uk-panel-box uk-panel-box-secondary">
                <div class="b-note-delete">
                    <a href="#" class="uk-button uk-button-mini uk-button-danger"><i class="uk-icon-close"></i></a>
                </div>
                <div class="b-note-date uk-badge uk-badge-info">
                    10.10.2016
                </div>
                <div class="b-note-text">
                    Текст заметки
                </div>
            </div><!-- .b-note -->
            <div class="b-note uk-panel uk-panel-box uk-panel-box-secondary">
                <div class="b-note-delete">
                    <a href="#" class="uk-button uk-button-mini uk-button-danger"><i class="uk-icon-close"></i></a>
                </div>
                <div class="b-note-date uk-badge uk-badge-info">
                    10.10.2016
                </div>
                <div class="b-note-text">
                    Текст заметки
                </div>
            </div><!-- .b-note -->
        </div><!-- .b-list.b-list_notes -->
        <div class="b-addNote">
            <form class="uk-form">
                <h3>Добавить новую заметку:</h3>
                <div class="uk-form-row">
                    <textarea></textarea>
                    <button class="uk-button uk-button-success" type="submit">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- .uk-modal -->

            <div class="uk-grid">
                <div class="uk-width-large-1-1">
                    <h2>
					<span class="uk-text-middle" id="dateText">
						<small><em>осень 2016 - 2017</em></small>
					</span>
                    </h2>

                    <div class="uk-alert" data-uk-alert>
                        <a href="" class="uk-alert-close uk-close"></a>
                        <p>В данном режиме просмотра расписания удобно просматривать загруженность той или иной недели для планирования дел</p>
                    </div>

                    <table id="table_subjects" class="b-table b-table_subjects uk-table">
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup class="m-active"></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <tbody class="b-table-head">
                        <tr>
                            <td></td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                            <td>10.09 - 17.09</td>
                        </tr>
                        <tr>
                            <th></th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th>13</th>
                            <th>14</th>
                            <th>15</th>
                            <th>16</th>
                        </tr>
                        </tbody><!-- .b-table-head -->
                        <tbody class="b-subject">
                        <tr class="b-subject-row i-subject-row" data-subject-day="1">
                            <td rowspan="6" class="b-subject-name">Экономика</td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="2">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-active">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="3">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="4">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="5">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="6">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        </tbody>
                        <tbody class="b-subject">
                        <tr class="b-subject-row i-subject-row" data-subject-day="1">
                            <td rowspan="6" class="b-subject-name">Экономика</td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="2">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-active">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="3">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="4">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="5">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="6">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        </tbody>
                        <tbody class="b-subject">
                        <tr class="b-subject-row i-subject-row" data-subject-day="1">
                            <td rowspan="6" class="b-subject-name">Экономика</td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="2">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-active">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="3">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="4">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="5">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="6">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        </tbody>
                        <tbody class="b-subject">
                        <tr class="b-subject-row i-subject-row" data-subject-day="1">
                            <td rowspan="6" class="b-subject-name">Экономика</td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="2">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-active">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="3">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="4">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="5">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="6">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        </tbody>
                        <tbody class="b-subject">
                        <tr class="b-subject-row i-subject-row" data-subject-day="1">
                            <td rowspan="6" class="b-subject-name">Экономика</td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="2">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-active">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="3">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="4">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="5">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="6">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        </tbody>
                        <tbody class="b-subject">
                        <tr class="b-subject-row i-subject-row" data-subject-day="1">
                            <td rowspan="6" class="b-subject-name">Экономика</td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="2">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-active">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="3">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lab">лаб</span>
								</span>
                            </td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="4">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="5">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        <tr class="b-subject-row i-subject-row" data-subject-day="6">
                            <td class="b-subject-name b-hidden"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                            <td class="b-subject-week i-subject-week">
								<span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_lecture">лек</span>
								</span>
                                <span class="b-lesson">
									<span class="b-lesson-type b-lesson-type_practice">пр</span>
								</span>
                            </td>
                            <td class="b-subject-week i-subject-week m-empty"></td>
                        </tr>
                        </tbody>
                    </table><!-- .b-subject -->

                    <script>
                        var $table_subjects = $("#table_subjects");
                        var subjectRow = $table_subjects.find(".i-subject-row");
                        var colgroup = $table_subjects.find("colgroup");

                        subjectRow.each(function() {
                            var $el = $(this);
                            var day = $el.data("subject-day");
                            var day_name;
                            var weeks = $el.find(".i-subject-week");

                            switch (day) {
                                case 1: day_name = "пн"; break;
                                case 2: day_name = "вт"; break;
                                case 3: day_name = "ср"; break;
                                case 4: day_name = "чт"; break;
                                case 5: day_name = "пт"; break;
                                case 6: day_name = "сб"; break;
                                case 7: day_name = "вс"; break;
                                default: day_name = "er"
                            }

                            weeks.append("<span class='b-subject-day'>"+day_name+"</span>");
                        });

                        $table_subjects.delegate('td','mouseover mouseleave', function(e) {
                            var $cell = $(this);
                            if (e.type == 'mouseover') {
                                $cell.parent().addClass("m-active");
                                colgroup.eq($cell.index()).addClass("m-hover");
                            } else {
                                $cell.parent().removeClass("m-active");
                                colgroup.eq($cell.index()).removeClass("m-hover");
                            }
                        });
                    </script>
                </div><!-- .uk-width-large-1-1 -->
            </div><!-- .uk-grid -->

            <!-- END CONTENT -->
            <!-- BEGIN FOOT -->
        </div><!-- .uk-container.b-main -->
    </div><!-- .l-main -->

    <div class="l-foot">
        <div class="l-foot-inner">
            <div class="b-foot">

            </div><!-- .b-foot -->
        </div><!-- .l-foot-inner -->
    </div><!-- .l-foot -->
