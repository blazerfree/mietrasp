<div class="uk-grid">
    <div class="uk-width-large-1-1">
        <h2>
            <a class="uk-button" id="prev_week" href="#">
                <i class="uk-icon-arrow-left"></i>
            </a>
            <span class="uk-text-middle" id="dateText">
                <?php
                use app\helpers\ThisDay;
                $date = isset($_GET['date']) ? $_GET['date'] : time();
                if (($nW = date('N', $date)) != 1)
                    $date -= ($nW - 1) * 24 * 60 * 60;
                $thisWeek = ThisDay::getThisWeek($date); ?>
                Неделя <?= $thisWeek['week'] ?>
                <small><em><?= date('j.n', $date) ?>
                        - <?= date('j.n', strtotime('+1 week', $date) - 2 * 24 * 60 * 60) ?></em></small>
            </span>
            <a class="uk-button" id="next_week" href="#">
                <i class="uk-icon-arrow-right"></i>
            </a>
            <input type="hidden" id="this_week_date" value="<?= isset($_GET['date']) ? $_GET['date'] : time() ?>"/>
            <input type="hidden" id="this_week" value="<?= $thisWeek['week'] ?>"/>
        </h2>

        <div class="b-list b-list_days b-list_days_week">

            <?php
            $parTime = [['09:00', '10:30'], ['10:40', '12:10'], ['12:20', '13:50'], ['14:20', '15:50'], ['16:00', '17:30'], ['17:40', '19:10'], ['19:20', '20:50']];
            $arrWeek = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

            foreach ($arrWeek as $keyWeek => $nameWeek) {
                ?>

                <div class="b-day <?= (date('N') - 1) == $keyWeek ? 'm-active' : '' ?>">
                <span class="b-day-week">
                    <?= date('j.n.Y', $date + $keyWeek * 24 * 60 * 60) ?>
                </span>
                    <div class="b-day-left">
                        <span class="b-day-title"><?= $nameWeek ?></span>
                    </div><!-- .b-day-left -->
                    <div class="b-list b-list_lessons">

                        <?php
                        if (count($arrScheduleitems[$keyWeek]) == 0) {
                            echo 'Нет занятий';
                        } else {

                            foreach ($arrScheduleitems[$keyWeek] as $one) {
                                ?>

                                <div class="b-lesson">
                                <div class="b-lesson-info">
                                    <div class="b-lesson-auditory"><?= $one['auditory'] ?></div>
                                </div>
                                <div class="b-lesson-content">
                                    <div class="b-lesson-title"><?= $one['discipline']['name'] ?></div>
                                    <div class="b-lesson-prepod"><?= $one['teacher']['fullName'] ?></div>
                                </div><!-- .b-lesson-content -->
                                <span class="b-lesson-pair"><?= $one['numberInDay'] ?></span>
                                <span class="b-lesson-timeBegin"><?= $parTime[$one['numberInDay'] - 1][0] ?></span>
                                <span class="b-lesson-timeEnd"><?= $parTime[$one['numberInDay'] - 1][1] ?></span>
                                </div>
                                <?php
                            }
                        } ?>
                    </div><!-- .b-list.b-list_lessons -->
                </div><!-- .b-day -->
            <?php } ?>

        </div><!-- .b-list.b-list_days -->
    </div><!-- .uk-width-large-1-1 -->
</div><!-- .uk-grid -->

<!-- END CONTENT -->