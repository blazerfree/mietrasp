<?php
use app\helpers\ThisDay;

$date = isset($_GET['date']) ? $_GET['date'] : time();
$thisWeek = ThisDay::getThisWeek($date); ?>


<div class="uk-container uk-container-center b-main">
    <!-- END HEAD -->
    <!-- BEGIN CONTENT -->

    <div class="uk-grid">
        <div class="uk-width-large-1-1">
            <h2>
                <a class="uk-button" id="dayrasp_prev" href="#">
                    <i class="uk-icon-arrow-left"></i>
                </a>
                <span class="uk-text-middle"
                      id="dateText"><?= $thDay['d'] . ' ' . $thDay['m'] . ' (' . $thDay['w'] . ')' ?></span>
                <input type="hidden" id="this_date" value="<?= $date ?>"/>
                <a class="uk-button" id="dayrasp_next" href="#">
                    <i class="uk-icon-arrow-right"></i>
                </a>
            </h2>

            <a id="yesterday" class="uk-button uk-button-small" href="#">вчера</a>
            <a id="today" class="uk-button uk-button-small uk-active" href="#">сегодня</a>
            <a id="tomorrow" class="uk-button uk-button-small" href="#">завтра</a>

            <div class="b-list b-list_days">
                <div class="b-day">
                    <span class="b-day-week"> <?= $thisWeek['type'].' '.date('j.n.Y', $date) ?></span>
                    <div class="b-day-left">
                        <span class="b-day-title"><?= $thDay['w']?></span>
                    </div><!-- .b-day-left -->
                    <div class="b-list b-list_lessons">

                        <?php
                        if (count($scheduleitems) == 0) {
                            echo 'Нет занятий';
                        } else {
                            $parTime = [['09:00', '10:30'], ['10:40', '12:10'], ['12:20', '13:50'], ['14:20', '15:50'], ['16:00', '17:30'], ['17:40', '19:10'], ['19:20', '20:50']];
                            foreach ($scheduleitems as $one) {
                                if ($one['notes']!=null && ($count = count($one['notes'])) != 0)
                                    $notes = json_encode($one['notes']);
                                else{
                                    $notes = 'null';
                                    $count = 0;
                                }
                                ?>

                                <div class="b-lesson">
                                <?php  if (!Yii::$app->user->isGuest && Yii::$app->getUser()->getIdentity()->student && Yii::$app->getUser()->getIdentity()->group==$_GET['group']) {?>
                                <a class="schedule" href="#note" data-uk-modal style="text-decoration: none;"
                                   onclick='showNotes("<?= $one["discipline"]["name"] ?>",<?= $notes.','.$one['discipline']['id'] ?>)'>
                            <?php }?>
                                <div class="b-lesson-info">
                                    <div class="b-lesson-auditory"><?= $one['auditory'] ?></div>
                                </div>
                                <div class="b-lesson-content">
                                    <div class="b-lesson-title"><?= $one['discipline']['name'] ?></div>
                                    <div class="b-lesson-prepod"><?= $one['teacher']['fullName'] ?></div>
                                    <div class="b-lesson-notes">
                                <?php  if (!Yii::$app->user->isGuest && Yii::$app->getUser()->getIdentity()->student && Yii::$app->getUser()->getIdentity()->group==$_GET['group']) {?>
                                            <i class="uk-icon-sticky-note"></i> <?= $count != 0 ? $count : '';?>
                                <?php }?>
                                    </div>
                                </div><!-- .b-lesson-content -->
                                <span class="b-lesson-pair"><?= $one['numberInDay'] ?></span>
                                <span class="b-lesson-timeBegin"><?= $parTime[$one['numberInDay'] - 1][0] ?></span>
                                <span class="b-lesson-timeEnd"><?= $parTime[$one['numberInDay'] - 1][1] ?></span>
                                <?php if (!Yii::$app->user->isGuest && Yii::$app->getUser()->getIdentity()->student && Yii::$app->getUser()->getIdentity()->group==$_GET['group']) ?>
                                    </a>
                                    </div>
                                    <?php
                            }
                        } ?>
                        <!-- .b-lesson -->
                        <!-- .b-lesson -->
                    </div><!-- .b-list.b-list_lessons -->
                </div><!-- .b-day -->
            </div><!-- .b-list.b-list_days -->
        </div><!-- .uk-width-large-1-1 -->
    </div><!-- .uk-grid -->
    <!-- END CONTENT -->