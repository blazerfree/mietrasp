<?php

namespace app\controllers;

use app\helpers\ThisDay;
use app\models\Note;
use app\models\Scheduleitem;
use app\models\Teacher;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\validators\Validator;
use yii\web\Controller;
use yii\helpers\Json;

use app\models\Rasp;
use app\models\Group;

class RaspController extends Controller
{
    public $layout = '@app/views/layouts/main_rasp.php';

    public $defaultAction = 'showRasp';


    public function actionShowRasp($type = 'day', $group = 'МП-34', $teacherName = 'Федоров Алексей Роальдович', $date = false)
    {

        if (!Yii::$app->user->isGuest && !Yii::$app->session->has('userName'))
            Yii::$app->session->set('userName', User::findOne(['id' => Yii::$app->user->getId()])->name);

        if (!$date)
            $time = static::thisDay();
        else
            $time = static::thisDay($date);

        $day = $time['day'];

        $period = $time['period'];

        //день неделя месяц
        if (!$date)
            $nDay = static::dwm();
        else
            $nDay = static::dwm($date);

        if (isset($_COOKIE['group']))
            $group = $_COOKIE['group'];

        $groupId = Group::find()->where(['name' => $group])->one();

        switch ($type) {
            case 'day':
                $scheduleitems = Scheduleitem::getScheduleitemDay($day, $period, $groupId);
                foreach ($scheduleitems as $key => $scheduleitem) {
                    if (!$scheduleitem['modPeriod']) {
                        unset($scheduleitems[$key]);
                    } elseif (!Yii::$app->user->isGuest && Yii::$app->getUser()->getIdentity()->student && Yii::$app->getUser()->getIdentity()->group == $group) {
                        $scheduleitems[$key]['notes'] = Note::getNotes($scheduleitem['discipline']['id']);
                    }else{
                        $scheduleitems[$key]['notes'] = null;
                    }
                }
                return $this->render('schedules/scheduleDay', ['scheduleitems' => $scheduleitems, 'week' => $time['week'], 'weekType' => $time['type'], 'thDay' => $nDay]);
                break;
            case 'week':
                $arrScheduleitems = [];
                for ($i = 0; $i < 6; $i++) {
                    $scheduleitems = Scheduleitem::getScheduleitemWeek($i, $period, $groupId);
                    foreach ($scheduleitems as $key => $scheduleitem) {
                        if (!$scheduleitem['modPeriod'])
                            unset($scheduleitems[$key]);
                    }
                    $arrScheduleitems[$i] = $scheduleitems;
                }
                return $this->render('schedules/scheduleWeek', ['arrScheduleitems' => $arrScheduleitems, 'week' => $time['week'], 'weekType' => $time['type'], 'thDay' => $nDay]);
                break;
            case 'month':

                $subjects = Scheduleitem::find()->select('disciplineId')->distinct()->where(['groupId' => $groupId])->with('discipline')->asArray()->all();
                
                foreach ($subjects as $sKey => $subj) {
                    $items = Scheduleitem::find()
                        ->where(['groupId' => $groupId, 'disciplineId' => $subj['discipline']['id']])
                        ->with(['discipline'])
                        ->orderBy('numberInDay')
                        ->asArray()
                        ->all();

                    for ($period = 8, $nWeek = 1; $period >= 1; $period /= 2, $nWeek++) {
                        for ($i = 0; $i < 7; $i++) {
                            foreach ($items as $itKey => $item){
                                if($i == $item['day'] && ($item['period'] & $period) != 0){
                                    $type = null;
                                    preg_match('/\[(([А-Яа-я]{6}\])|([А-Яа-яр]{4}\]))/', $item['discipline']['name'], $type);
                                    $subjects[$sKey]['days'][$nWeek][$item['day']] = empty($type) ? null : str_replace(['[', ']'], '', $type[0]);
                                }
                            }
                        }
                    }
                }
                
                $unset = [];
                foreach ($subjects as $sKey => $subj) {
                    $type = null;
                    preg_match('/\[(([А-Яа-я]{6}\])|([А-Яа-яр]{4}\]))/', $subj['discipline']['name'], $type);
                    if ($type)
                        $type = $type[0];
                    else continue;
                    $subjectName = str_replace(['[Лек]', '[Пр]', '[Лаб]'], '', $subj['discipline']['name']);
                    $type = str_replace(['[', ']'], '', $type);
                    $type = mb_strtolower($type, 'utf8');
                    $subjects[$sKey][$type] = $subjects[$sKey]['days'];
                    foreach ($subjects as $sKey2 => $subj2) {
                        $type2 = null;
                        preg_match('/\[(([А-Яа-я]{6}\])|([А-Яа-яр]{4}\]))/', $subj2['discipline']['name'], $type2);
                        if ($type2)
                            $type2 = $type2[0];
                        else continue;
                        $type2 = str_replace(['[', ']'], '', $type2);
                        $type2 = mb_strtolower($type2, 'utf8');
                        $subjectName2 = str_replace(['[Лек]', '[Пр]', '[Лаб]'], '', $subj2['discipline']['name']);

                        if ($sKey2 > $sKey && $subjectName == $subjectName2) {
                            $subjects[$sKey][$type2] = $subjects[$sKey2]['days'];
                            $unset[] = $sKey2;
                        }

                        if ($subjectName2 == 'Микропроцессорные средства и системы ' && $subjectName == 'Архитектура микропроцессорных систем и средств/Микропроцессорные средства и системы ') {
                            $subjects[$sKey][$type2] = $subjects[$sKey2]['days'];
                            $unset[] = $sKey2;
                        }
                    }
                    $subjects[$sKey]['discipline']['name'] = str_replace([' [Лек]', ' [Пр]', ' [Лаб]'], '', $subjects[$sKey]['discipline']['name']);
                    unset($subjects[$sKey]['days']);
                }
                foreach ($unset as $val) {
                    unset($subjects[$val]);
                }

                return $this->render('schedules/scheduleMonth', ['subjects' => $subjects, 'week' => $time['week'], 'weekType' => $time['type'], 'thDay' => $nDay]);
                break;
            case 'teacher':
                return $this->scheduleTeacher($teacherName,$time,$nDay,$day);
                break;
            default:
                throw new Exception('Неверный тип расписания', 400);
        }
    }
    
    private function scheduleTeacher($teacherName,$time,$nDay,$dayOfWeek){
        $teacher = Teacher::find()->select('id')->where(['fullName'=>$teacherName])->one();
        if(empty($teacher))
            throw new Exception('Преподаватель не найден', 400);
        $subjects = Scheduleitem::find()->select('disciplineId')->distinct()->where(['teacherId' => $teacher->id])->with('discipline')->asArray()->all();

        foreach ($subjects as $sKey => $subj) {
            $items = Scheduleitem::find()
                ->where(['teacherId' => $teacher->id, 'disciplineId' => $subj['discipline']['id']])
                ->with(['discipline'])
                ->orderBy('numberInDay')
                ->asArray()
                ->all();

            for ($period = 8, $nWeek = 1; $period >= 1; $period /= 2, $nWeek++) {
                for ($i = 0; $i < 7; $i++) {
                    foreach ($items as $itKey => $item){
                        if($i == $item['day'] && ($item['period'] & $period) != 0){
                            $type = null;
                            preg_match('/\[(([А-Яа-я]{6}\])|([А-Яа-яр]{4}\]))/', $item['discipline']['name'], $type);
                            $subjects[$sKey]['days'][$nWeek][$item['day']] = empty($type) ? null : str_replace(['[', ']'], '', $type[0]);
                        }
                    }
                }
            }
        }

        $unset = [];
        foreach ($subjects as $sKey => $subj) {
            $type = null;
            preg_match('/\[(([А-Яа-я]{6}\])|([А-Яа-яр]{4}\]))/', $subj['discipline']['name'], $type);
            if ($type)
                $type = $type[0];
            else continue;
            $subjectName = str_replace(['[Лек]', '[Пр]', '[Лаб]'], '', $subj['discipline']['name']);
            $type = str_replace(['[', ']'], '', $type);
            $type = mb_strtolower($type, 'utf8');
            $subjects[$sKey][$type] = $subjects[$sKey]['days'];
            foreach ($subjects as $sKey2 => $subj2) {
                $type2 = null;
                preg_match('/\[(([А-Яа-я]{6}\])|([А-Яа-яр]{4}\]))/', $subj2['discipline']['name'], $type2);
                if ($type2)
                    $type2 = $type2[0];
                else continue;
                $type2 = str_replace(['[', ']'], '', $type2);
                $type2 = mb_strtolower($type2, 'utf8');
                $subjectName2 = str_replace(['[Лек]', '[Пр]', '[Лаб]'], '', $subj2['discipline']['name']);

                if ($sKey2 > $sKey && $subjectName == $subjectName2) {
                    $subjects[$sKey][$type2] = $subjects[$sKey2]['days'];
                    $unset[] = $sKey2;
                }

                if ($subjectName2 == 'Микропроцессорные средства и системы ' && $subjectName == 'Архитектура микропроцессорных систем и средств/Микропроцессорные средства и системы ') {
                    $subjects[$sKey][$type2] = $subjects[$sKey2]['days'];
                    $unset[] = $sKey2;
                }
            }
            $subjects[$sKey]['discipline']['name'] = str_replace([' [Лек]', ' [Пр]', ' [Лаб]'], '', $subjects[$sKey]['discipline']['name']);
            unset($subjects[$sKey]['days']);
        }
        foreach ($unset as $val) {
            unset($subjects[$val]);
        }

        /*
         * местонахождение
         */
        $thisWeek = ThisDay::getThisWeek();
        $week = isset($_GET['fweek']) ? $_GET['fweek'] : $thisWeek['week'];
        $day = isset($_GET['fday']) ? $_GET['fday'] : $dayOfWeek;
        $pare = isset($_GET['fp']) ? $_GET['fp'] : 1;


        $whereScheduleItem = Scheduleitem::find()->select([
            '{{scheduleitems}}.*',
            '([[period]] & '.(static::getPeriodForWeek($week)).') AS modPeriod',
        ])->where(['teacherId'=>$teacher->id,'day'=>$day,'numberInDay'=>$pare])->all();

        $whereTeacher = !empty($whereScheduleItem) ? $whereScheduleItem : null;

        $this->layout = '@app/views/layouts/schedule_teacher.php';
        return $this->render('schedules/scheduleTeacher', ['subjects' => $subjects, 'week' => $time['week'], 'weekType' => $time['type'], 'thDay' => $nDay,'whereTeacher'=>$whereTeacher]);
    }

    public function actionWhereTeacher($week,$day,$pare,$teacherName){

        if(!is_numeric($week)||!is_numeric($day)||!is_numeric($pare)||preg_match('[^А-Яа-яеёЁЕ\s]',$teacherName))
            return 'error';

        $teacher = Teacher::find()->where(['fullName'=>$teacherName])->one();

        $whereScheduleItem = Scheduleitem::find()->select([
            '{{scheduleitems}}.*',
            '([[period]] & '.(static::getPeriodForWeek($week)).') AS modPeriod',
        ])->where(['teacherId'=>$teacher->id,'day'=>$day,'numberInDay'=>$pare])->asArray()->all();

        $i = 0;
        foreach ($whereScheduleItem as $item) {
            if($item['modPeriod']==0){
                unset($whereScheduleItem[$i]);
            }else{
                $whereScheduleItem = $whereScheduleItem[$i];
                break;
            }
            $i++;
        }

        return !empty($whereScheduleItem) ? $whereScheduleItem['auditory'] : null;
    }
    /**
     * @return Action
     */
    public function actionItem()
    {
        $item = Scheduleitem::find(['id' => 5011])->with(['group', 'discipline', 'teacher'])->one();
        var_dump($item->group->name);
        return 0;
    }

    private static function dwm($date = false)
    {
        if (!$date)
            $date = time();
        $d = date('j', $date);
        switch (date('N', $date)) {
            case 1:
                $w = 'понедельник';
                break;
            case 2:
                $w = 'вторник';
                break;
            case 3:
                $w = 'среда';
                break;
            case 4:
                $w = 'четверг';
                break;
            case 5:
                $w = 'пятница';
                break;
            case 6:
                $w = 'суббота';
                break;
            case 7:
                $w = 'воскресенье';
                break;
        }
        switch (date('n', $date)) {
            case 1:
                $m = 'Января';
                break;
            case 2:
                $m = 'Февраля';
                break;
            case 3:
                $m = 'Марта';
                break;
            case 4:
                $m = 'Апреля';
                break;
            case 5:
                $m = 'Мая';
                break;
            case 6:
                $m = 'Июня';
                break;
            case 7:
                $m = 'Июля';
                break;
            case 8:
                $m = 'Августа';
                break;
            case 9:
                $m = 'Сентября';
                break;
            case 10:
                $m = 'Октября';
                break;
            case 11:
                $m = 'Ноября';
                break;
            case 12:
                $m = 'Декабря';
                break;
        }
        return ['d' => $d, 'w' => $w, 'm' => $m];
    }

    public static function thisDay($date = false)
    {
        if (!$date)
            $date = time();
        $thisWeek = date('W', $date);
        $thisDay = date('N', $date);
        if ($thisWeek >= 35 && $thisWeek <= 53)
            $thisWeek -= 34;
        else
            $thisWeek -= 5;

        $period = '';
        $type = '';
        switch ($thisWeek % 4) {
            case 1:
                $period = '8'; //1 числ
                $type = '1 числитель';
                break;
            case 2:
                $period = '4'; //1 знам
                $type = '1 знаменатель';
                break;
            case 3:
                $period = '2';//2 числ
                $type = '2 числитель';
                break;
            case 0:
                $period = '1';//2 знам
                $type = '2 знаменатель';
                break;

        }

        return ['day' => $thisDay, 'period' => $period, 'week' => $thisWeek, 'type' => $type];
    }

    private static function getPeriodForWeek($week){
        $period = null;
        switch ($week % 4) {
            case 1:
                $period = '8'; //1 числ
                break;
            case 2:
                $period = '4'; //1 знам
                break;
            case 3:
                $period = '2';//2 числ
                break;
            case 0:
                $period = '1';//2 знам
                break;
        }
        return $period;
    }
}