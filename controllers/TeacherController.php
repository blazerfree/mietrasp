<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.11.2016
 * Time: 21:41
 */

namespace app\controllers;
use app\models\Group;
use app\models\Message;
use yii\web\Controller;
use Yii;

class TeacherController extends Controller
{
    public $layout = '@app/views/layouts/message.php';

    public $defaultAction = 'index';

    public function actionIndex(){
        return $this->render('/teacher/prepod_send.php');
    }

    public function actionUserMessage(){

        if(Yii::$app->getUser()->isGuest){
            Yii::$app->session->addFlash('err','Сначала войдите');
            return $this->goBack();
        }

        if(!Yii::$app->getUser()->getIdentity()->student){
            Yii::$app->session->addFlash('err','Вы не студент');
            return $this->goBack();
        }

        $userGroup = Yii::$app->getUser()->getIdentity()->group;

        $messages = Message::find()->where(['group'=>$userGroup])->with(['group','teacher'])->asArray()->all();

        return $this->render('/teacher/students_msg.php',['messages'=>$messages]);
    }

    public function actionSendMessage(){
        if(Yii::$app->getUser()->isGuest){
            Yii::$app->session->addFlash('err','Сначала войдите');
            return $this->goBack();
        }

        if(Yii::$app->getUser()->getIdentity()->student){
            Yii::$app->session->addFlash('err','Вы не преподаватель');
            return $this->goBack();
        }

        $request = $_REQUEST;

        $message = new Message();
        $message->group = $request['group'];
        $message->user_id = \Yii::$app->getUser()->getId();
        $message->text = $request['text'];

        if(!$message->save()){
            Yii::$app->session->addFlash('err','Ошибка валиданции');
            return $this->goBack();
        }

        $this->goBack();

    }

    public function actionNewMessages(){
        if(Yii::$app->getUser()->isGuest){
            Yii::$app->session->addFlash('err','Сначала войдите');
            return $this->goBack();
        }
        $userGroup = Yii::$app->getUser()->getIdentity()->group;
        $messages = Message::find()->where(['group'=>$userGroup])->andWhere(['>','created_at',time()-24*60*60])->asArray()->all();
        return count($messages);
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent :: beforeAction($action);
    }
}