<?php

namespace app\controllers;

use app\models\User;
use yii\base\DynamicModel;
use yii\web\Controller;
use Yii;
use yii\web\Session;

class AuthController extends Controller
{

    public function actionIndex(){
        return $this->redirect('/reg_login.html');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            Yii::$app->session->addFlash('err','Вы уже вошли');
            return $this->goBack();
        }

        //получение данных
        $email = \Yii::$app->request->post('email');
        $password = \Yii::$app->request->post('password');

        //валидация
        $validator = DynamicModel::validateData(['email'=>$email,'password'=>$password],[
            ['password','required'],
            ['email','email']
        ]);
        if($validator->hasErrors()){
            Yii::$app->session->addFlash('err','Данные не прошли валидацию');
            return $this->goBack();
        }

        $user = User::findOne(['email'=>$email]);
        if($user!=null) {
            if (Yii::$app->getSecurity()->validatePassword($password, $user->password_hash)) {
                \Yii::$app->user->login($user);
                return $this->goBack();
            }
        }

        Yii::$app->session->addFlash('err','Пользователь не найден');
        return $this->goBack();

    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Registration
     */
    public function actionRegistr()
    {
        $request = $_REQUEST;
        
        $user = new User();
        $user->name =$request['name'];
        $user->email= $request['email'];
        $user->group= $request['group'];
        $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($request['password']);
        if(!$request['student']){
            if($request['key']!='123456') {
                Yii::$app->session->addFlash('err', 'Неверный ключ');
                return $this->goBack();
            }
        }
        $user->student = $request['student'];
        $user->created_at = time();

        if(!$user->save()){
            Yii::$app->session->addFlash('err','Ошибка валиданции');
            return $this->goBack();
        }
        return $this->goBack();

    }


    //получить токен
    public function actionToken(){
        return \yii\helpers\Html::csrfMetaTags();
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent :: beforeAction($action);
    }
}
