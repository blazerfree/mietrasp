<?php
/**
 * Created by PhpStorm.
 * User: dean
 * Date: 01.11.16
 * Time: 20:45
 */

namespace app\controllers;
use app\models\Note;

use yii\web\Controller;
use Yii;
class NotesController extends Controller
{

    /**
     * @return bool
     */
    public function actionCreate(){
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->addFlash('err','Сначала надо войти');
            return $this->goBack();
        }

        $request = $_REQUEST;

        $note = new Note();
        $note->user_id = \Yii::$app->getUser()->getId();
        $note->discipline_id = $request['disciplineId'];
        //$note->title = $request['title'];
        $note->text = $request['text'];

        if(!$note->save()){
            Yii::$app->session->addFlash('err','Ошибка валиданции');
            return $this->goBack();
        }

        $this->goBack();
    }

    /**
     * @param $id
     */
    public function actionDeleteNote($id){
        Note::deleteAll(['id'=>$id]);
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionUpdate($id){

        $request = $_REQUEST;

        $note = Note::findOne(['id'=>$id]);
        if(isset($request['title']))
            $note->title = $request['title'];
        if(isset($request['text']))
            $note->text = $request['text'];
        $note->updated_at = time();

        return $note->save();

    }

    /**
     * @param $subjectId
     * @return bool|int
     */
    public function actionDeleteSubjectNotes($subjectId){
        return Note::deleteSubjectNotes($subjectId);
    }

    /**
     * @param $subjectId
     * @return \yii\db\ActiveQuerygit
     */
    public function getNotes($subjectId){
        return Note::getNotes($subjectId);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent :: beforeAction($action);
    }

}