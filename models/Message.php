<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group
 * @property string $text
 * @property integer $created_at
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group', 'text', 'created_at'], 'required'],
            [['user_id',  'created_at'], 'integer'],
            [['text'], 'string', 'max' => 1000],
            [['group'],'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group' => 'Group',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }

    public function __construct()
    {
        parent::__construct();

        $this->created_at = time();
    }

    public function getgroup()
    {
        return $this->hasOne(Group::className(),['name'=>'group']);
    }
    public function getteacher()
    {
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }
}
