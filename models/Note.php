<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $discipline_id
 * @property string $title
 * @property string $text
 * @property integer $complete
 * @property integer $created_at
 * @property integer $updated_at
 */
class Note extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'discipline_id', 'text', 'created_at'], 'required'],
            [['user_id', 'discipline_id', 'complete', 'created_at', 'updated_at'], 'integer'],
            [['title', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'discipline_id' => 'Discipline_id ID',
            'title' => 'Title',
            'text' => 'Text',
            'complete' => 'Complete',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Note constructor.
     * @param array $config
     */
    public function __construct()
    {
        parent::__construct();

        $this->created_at = time();
    }

    /**
     * @param $scheduleitemId
     * @return bool|int
     */
    public static function deleteSubjectNotes($scheduleitemId){
        $validator = self::validate(['subject_id'=>$scheduleitemId],['sbject_id','integer']);
        if($validator->hasErrors()){
            return false;
        }
        return self::deleteAll(['subject_id'],[$scheduleitemId]);
    }

    /**
     * @param $scheduleitemId
     * @return \yii\db\ActiveQuery
     */
    public static function getNotes($disciplineId){
        return self::find()->where(['discipline_id'=>$disciplineId,'user_id'=>Yii::$app->getUser()->getId()])->asArray()->all();
    }
}
