<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scheduleitems".
 *
 * @property integer $id
 * @property integer $groupId
 * @property integer $disciplineId
 * @property integer $day
 * @property integer $numberInDay
 * @property string $auditory
 * @property integer $period
 * @property integer $teacherId
 */
class Scheduleitem extends \yii\db\ActiveRecord
{
    /**
     * @var
     * Модифицированный период (period & x) 
     */
    public $modPeriod;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheduleitems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scheduleitem_id','groupId', 'disciplineId', 'day', 'numberInDay', 'period', 'teacherId'], 'integer'],
            [['auditory'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'groupId' => 'Group ID',
            'disciplineId' => 'Discipline ID',
            'day' => 'Day',
            'numberInDay' => 'Number In Day',
            'auditory' => 'Auditory',
            'period' => 'Period',
            'teacherId' => 'Teacher ID',
            'scheduleitem_id' => 'Scheduleitem ID'
        ];
    }


    public static function getScheduleitemDay($day,$period,$groupId){
        return static::find()
            ->select([
                '{{scheduleitems}}.*',
                '([[period]] & '.$period.') AS modPeriod',
            ])
            ->where(['day'=>$day,'groupId'=>$groupId])
            ->with(['group','discipline','teacher'])
            ->orderBy('numberInDay')
            ->asArray()
            ->all();
    }

    public static function getScheduleitemWeek($i,$period,$groupId){
        return static::find()
            ->select([
                '{{scheduleitems}}.*',
                '([[period]] & ' . $period . ') AS modPeriod',
            ])
            ->where(['groupId' => $groupId,'day' => ($i + 1)])
            ->with(['group', 'discipline', 'teacher'])
            ->orderBy('numberInDay')
            ->asArray()
            ->all();
    }
    
    public function getgroup()
    {
        return $this->hasOne(Group::className(),['id'=>'groupId']);
    }

    public function getdiscipline(){
        return $this->hasOne(Discipline::className(),['id'=>'disciplineId']);
    }

    public function getteacher(){
        return $this->hasOne(Teacher::className(),['id'=>'teacherId']);
    }

}
