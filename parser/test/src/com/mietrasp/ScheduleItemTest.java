package src.com.mietrasp;

import com.mietrasp.api.DisciplineApiImpl;
import com.mietrasp.api.GroupApiImpl;
import com.mietrasp.api.ScheduleItemApiImpl;
import com.mietrasp.api.TeacherApiImpl;
import org.junit.Test;

import static org.junit.Assert.*;

public class ScheduleItemTest {
    private final GroupApiImpl GROUP = new GroupApiImpl("dummy group");
    private final TeacherApiImpl TEACHER = new TeacherApiImpl("Dummy teacher", "dummy");
    private final DisciplineApiImpl DISCIPLINE = new DisciplineApiImpl("dummy discipline");


    @Test
    public void itemRepeatsEveryFirstWeek(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 1, 8, "asd");
        scheduleItem.setRepeatsEveryFirstWeek();

        assertEquals(true, scheduleItem.repeatsEveryFirstWeek());
    }

    @Test
    public void itemRepeatsEverySecondWeek(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 1, 8, "asd");
        scheduleItem.setRepeatsEverySecondWeek();

        assertEquals(true, scheduleItem.repeatsEverySecondWeek());
    }

    @Test
    public void itemRepeatsEveryThirdWeek(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 1, 8, "asd");
        scheduleItem.setRepeatsEveryThirdWeek();

        assertEquals(true, scheduleItem.repeatsEveryThirdWeek());
    }

    @Test
    public void itemRepeatsEveryFourthWeek(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 1, 8, "asd");
        scheduleItem.setRepeatsEverySecondWeek();

        assertEquals(true, scheduleItem.repeatsEverySecondWeek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionThrownWhenInvalidDayProvided(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 0, 8, "asd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionThrownWhenInvalidDayProvided2(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 0, 0, "asd");
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionThrownWhenInvalidNumberInDayProvided(){
        ScheduleItemApiImpl scheduleItem = new ScheduleItemApiImpl(GROUP, TEACHER, DISCIPLINE, 0, 5, "asd");
    }
}
