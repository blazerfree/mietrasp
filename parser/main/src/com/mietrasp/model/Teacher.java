package com.mietrasp.model;

/**
 * Represents a teacher in miet
 */
public interface Teacher {

    /**
     * Returns short name of teacher
     * @return String
     */
    String getShortName();

    /**
     * Returns full name of teacher
     * @return String
     */
    String getFullName();
}
