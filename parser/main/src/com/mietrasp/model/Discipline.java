package com.mietrasp.model;

/**
 * Represents discipline in university
 */
public interface Discipline {

    /**
     * Returns full name of discipline
     * @return String
     */
    String getName();
}
