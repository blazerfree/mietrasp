package com.mietrasp.model;

/**
 * Represents a group of students in miet
 */
public interface Group {

    /**
     * Returns name of the group
     * @return String
     */
    String getName();
}
