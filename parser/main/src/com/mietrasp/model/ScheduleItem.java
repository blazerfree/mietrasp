package com.mietrasp.model;

/**
 * Represents one schedule item
 */
public interface ScheduleItem {

    /**
     * returns true if this item takes place every first week in the month
     * @return boolean
     */
    boolean repeatsEveryFirstWeek();

    /**
     * returns true if this item takes place every second week in the month
     * @return boolean
     */
    boolean repeatsEverySecondWeek();

    /**
     * returns true if this item takes place every third week in the month
     * @return boolean
     */
    boolean repeatsEveryThirdWeek();

    /**
     * returns true if this item takes place every fourth week in the month
     * @return boolean
     */
    boolean repeatsEveryFourthWeek();

    /**
     * Returns {@link Teacher} associated with this item
     * @return {@link Teacher}
     */
    Teacher getTeacher();

    /**
     * Returns {@link Group} associated with this item
     * @return {@link Group}
     */
    Group getGroup();

    /**
     * Returns {@link Discipline} associated with this schedule item
     * @return Discipline
     */
    Discipline getDiscipline();

    /**
     * Returns a number of day when this item takes a place
     * 1 for monday
     * 2 for tuesday
     * ...
     * 7 for sunday
     * @return int
     */
    int getDay();

    /**
     * Returns ordinal number in day for this item
     * @return int
     */
    int getNumberInDay();

    /**
     * Returns String representation of auditory where this item takes a place
     * @return String
     */
    String getAuditory();
}
