package com.mietrasp.api;

/**
 * Represents a discipline in miet
 */
public final class DisciplineApiImpl implements com.mietrasp.model.Discipline {
    private final String name;

    public DisciplineApiImpl(String name){
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisciplineApiImpl that = (DisciplineApiImpl) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "DisciplineApiImpl{" +
                "name='" + name + '\'' +
                '}';
    }
}
