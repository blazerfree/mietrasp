package com.mietrasp.api;


import com.mietrasp.model.Group;

import java.io.IOException;
import java.util.Set;

public interface ScheduleApi {

    /**
     * Returns a set of groups which are present in the university
     * @return Set
     * @throws IOException if any connection problem occurred
     */
    Set<Group> getGroups() throws IOException;

    /**
     * Returns a schedule for semester for provided group
     * @param group to get schedule for
     * @return {@link ScheduleResponse}
     * @throws IOException if any connection problem occurred
     */
    ScheduleResponse getScheduleByGroup(Group group) throws IOException;
}
