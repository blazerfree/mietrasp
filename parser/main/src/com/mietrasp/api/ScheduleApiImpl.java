package com.mietrasp.api;


import com.mietrasp.model.Discipline;
import com.mietrasp.model.Group;
import com.mietrasp.model.ScheduleItem;
import com.mietrasp.model.Teacher;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * {@link ScheduleApi} implementation
 * {@see {@link ScheduleApi}}
 */
public class ScheduleApiImpl implements ScheduleApi {
    /**
     * A base url for accessing schedule api
     */
    private static final String BASE_SCHEDULE_URL = "https://miet.ru/schedule";
    /**
     * A groups url path for accessing Groups list.
     *
     * Sending GET request to {@code BASE_SCHEDULE_URL + GROUPS_PATH}
     * will return json array of groups.
     */
    private static final String GROUPS_PATH = "/groups";
    /**
     * A schedule url path for accessing schedule by group
     * POST {'group' : GROUP_NAME} to {@code BASE_SCHEDULE_URL + DATA_PATH}
     * to get schedule for group
     */
    private static final String DATA_PATH = "/data";

    private static final int CONNECTION_TIMEOUT
            = (int)TimeUnit.SECONDS.toMillis(40);

    @Override
    public Set<Group> getGroups() throws IOException {
        Set<Group> set = new HashSet<>();
        // prepare connection
        URL url = new URL(BASE_SCHEDULE_URL + GROUPS_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(CONNECTION_TIMEOUT);
        conn.setDoInput(true);

        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null){
                sb.append(line);
            }

            // process response
            JSONArray jsonArray = new JSONArray(sb.toString());
            int length = jsonArray.length();
            for(int i = 0; i < length; i++){
                set.add(new GroupApiImpl(jsonArray.getString(i)));
            }

            return set;
        }
    }

    @Override
    public ScheduleResponse getScheduleByGroup(Group group) throws IOException {
        // prepare connection
        URL url = new URL(BASE_SCHEDULE_URL + DATA_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setConnectTimeout(CONNECTION_TIMEOUT);
        conn.setDoOutput(true);
        conn.setDoInput(true);

        // POST data
        try (BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()))) {
            wr.write("group=" + group.getName());
            wr.flush();

            // get response
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))){
                StringBuilder sb = new StringBuilder(1000);
                String line;

                while ((line = br.readLine()) != null){
                    sb.append(line);
                }

                // process it
                return processScheduleResponse(group, new JSONObject(sb.toString()));
            }

        }

    }

    private static ScheduleResponse processScheduleResponse(Group group, JSONObject json){
        Set<Teacher> teachers = new HashSet<>();
        Set<Discipline> disciplines = new HashSet<>();
        Set<ScheduleItem> scheduleItems = new HashSet<>();

        JSONArray data = json.getJSONArray("Data");
        int length = data.length();
        for(int i = 0; i < length; i++){
            JSONObject classJson = data.getJSONObject(i).getJSONObject("Class");
            TeacherApiImpl teacher = new TeacherApiImpl(classJson.getString("Teacher"), classJson.getString("TeacherFull"));
            teachers.add(teacher);
            DisciplineApiImpl discipline = new DisciplineApiImpl(classJson.getString("Name"));
            disciplines.add(discipline);

            /*
             * String like '2 пара' is parsed.
             * Since correct ordinal schedule item contains only here
             */
            String numInDayStr = data.getJSONObject(i).getJSONObject("Time").getString("Time");
            int numInDay = Integer.parseInt(numInDayStr.substring(0, numInDayStr.indexOf(' ')));

            String auditory = data.getJSONObject(i).getJSONObject("Room").getString("Name");

            int day = data.getJSONObject(i).getInt("Day");

            /*
             * Api returns several items which differs only by period.
             * We want to prevent duplicates. So we will construct only one item and then
             * check whether do we have already constructed this item and if we did, just update period
             */
            ScheduleItemApiImpl scheduleItem = null;

            boolean found = false;
            for(ScheduleItem item : scheduleItems){
                if (item.getAuditory().equals(auditory)
                        && item.getDay() == day
                        && item.getDiscipline().equals(discipline)
                        && item.getNumberInDay() == numInDay){
                    found = true;
                    scheduleItem = (ScheduleItemApiImpl) item;
                    break;
                }
            }
            if (!found){
                // not found, create
                scheduleItem = new ScheduleItemApiImpl((GroupApiImpl)group, teacher, discipline, day, numInDay, auditory);
                scheduleItems.add(scheduleItem);
            }
            int period = data.getJSONObject(i).getInt("DayNumber");
            switch (period){
                case 0:
                    scheduleItem.setRepeatsEveryFirstWeek();
                    break;
                case 1:
                    scheduleItem.setRepeatsEverySecondWeek();
                    break;
                case 2:
                    scheduleItem.setRepeatsEveryThirdWeek();
                    break;
                case 3:
                    scheduleItem.setRepeatsEveryFourthWeek();
                    break;
            }
        }


        return new ScheduleResponse(teachers,scheduleItems, disciplines, group);
    }
}
