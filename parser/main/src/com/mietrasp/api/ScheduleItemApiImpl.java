package com.mietrasp.api;

import com.mietrasp.model.Discipline;
import com.mietrasp.model.Group;
import com.mietrasp.model.ScheduleItem;
import com.mietrasp.model.Teacher;

public class ScheduleItemApiImpl implements ScheduleItem {

    private final GroupApiImpl group;

    private final TeacherApiImpl teacher;

    private final DisciplineApiImpl discipline;

    private final int day;

    private final int numberInDay;

    private final String auditory;

    // masks
    private static final int REPEATS_EVERY_FIRST_WEEK = 1 << 3;
    private static final int REPEATS_EVERY_SECOND_WEEK = 1 << 2;
    private static final int REPEATS_EVERY_THIRD_WEEK = 1 << 1;
    private static final int REPEATS_EVERY_FOURTH_WEEK = 1;
    /**
     * {@see {@link #repeatsEveryFourthWeek()}}
     * {@see {@link #repeatsEveryThirdWeek()}
     * {@see {@link #repeatsEverySecondWeek()}}
     * {@see {@link #repeatsEveryFirstWeek()}}
     */
    private int period;

    public ScheduleItemApiImpl(GroupApiImpl group, TeacherApiImpl teacher, DisciplineApiImpl discipline, int day, int numberInDay, String auditory) {
        this.group = group;
        this.teacher = teacher;
        this.discipline = discipline;
        if (day <= 0 || day > 7) {
            throw new IllegalArgumentException("1 for monday... 7 for sunday. Provided: " + day);
        }
        this.day = day;
        if (numberInDay <= 0) {
            throw new IllegalArgumentException("Item cannot take place at zero or negative number in day.");
        }
        this.numberInDay = numberInDay;
        this.auditory = auditory;
    }

    @Override
    public Teacher getTeacher() {
        return teacher;
    }

    public Group getGroup() {
        return group;
    }

    @Override
    public Discipline getDiscipline() {
        return discipline;
    }

    public void setRepeatsEveryFirstWeek(){
        period |= REPEATS_EVERY_FIRST_WEEK;
    }

    public void setRepeatsEverySecondWeek(){
        period |= REPEATS_EVERY_SECOND_WEEK;
    }

    public void setRepeatsEveryThirdWeek(){
        period |= REPEATS_EVERY_THIRD_WEEK;
    }

    public void setRepeatsEveryFourthWeek(){
        period |= REPEATS_EVERY_FOURTH_WEEK;
    }

    public boolean repeatsEveryFirstWeek() {
        return (period & REPEATS_EVERY_FIRST_WEEK) != 0;
    }

    public boolean repeatsEverySecondWeek() {
        return (period & REPEATS_EVERY_SECOND_WEEK) != 0;
    }

    public boolean repeatsEveryThirdWeek() {
        return (period & REPEATS_EVERY_THIRD_WEEK) != 0;
    }

    public boolean repeatsEveryFourthWeek() {
        return (period & REPEATS_EVERY_FOURTH_WEEK) != 0;
    }

    public int getDay() {
        return day;
    }

    public int getNumberInDay() {
        return numberInDay;
    }

    public String getAuditory() {
        return auditory;
    }
}
