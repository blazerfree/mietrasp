package com.mietrasp.api;


public final class GroupApiImpl implements com.mietrasp.model.Group {
    private final String name;

    public GroupApiImpl(String name){
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupApiImpl groupApiImpl = (GroupApiImpl) o;

        return name.equals(groupApiImpl.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "GroupApiImpl{" +
                "name='" + name + '\'' +
                '}';
    }
}
