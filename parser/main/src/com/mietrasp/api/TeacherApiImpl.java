package com.mietrasp.api;

public class TeacherApiImpl implements com.mietrasp.model.Teacher {
    private final String shortName;
    private final String fullName;

    public TeacherApiImpl(String shortName, String fullName) {
        this.shortName = shortName;
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "TeacherApiImpl{" +
                "shortName='" + shortName + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public String getFullName() {
        return fullName;
    }
}
