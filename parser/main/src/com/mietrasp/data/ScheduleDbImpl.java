package com.mietrasp.data;

import com.mietrasp.model.Discipline;
import com.mietrasp.model.Group;
import com.mietrasp.model.ScheduleItem;
import com.mietrasp.model.Teacher;

import java.sql.*;
import java.util.Collection;

public class ScheduleDbImpl implements ScheduleDb {
    private static final String JDBC_URL_STR_FMT = "jdbc:mysql://localhost/%s?serverTimezone=UTC";

    private final String jdbcDbUrl;
    private final String username;
    private final String password;

    public ScheduleDbImpl(String dbName, String username, String password) {
        this.jdbcDbUrl = String.format(JDBC_URL_STR_FMT, dbName);
        this.username = username;
        this.password = password;
    }

    /**
     * {@link Group}
     */
    private static class Groups{
        static final String TABLE_NAME = "groups";
        // integer, primary key auto_increment
        static final String KEY_ID = "id";

        // text
        static final String KEY_NAME = "name";
    }

    /**
     * {@link Teacher}
     */
    private static class Teachers{
        static final String TABLE_NAME = "teachers";

        // integer, primary key auto_increment
        static final String KEY_ID = "id";

        // text
        static final String KEY_SHORT_NAME = "shortName";

        // text
        static final String KEY_FULL_NAME = "fullName";
    }

    /**
     * {@link Discipline}
     */
    private static class Disciplines{
        static final String TABLE_NAME = "disciplines";

        // integer, primary key auto_increment
        static final String KEY_ID = "id";

        // text
        static final String KEY_NAME = "name";
    }

    /**
     * {@link ScheduleItem}
     */
    private static class ScheduleItems{
        static final String TABLE_NAME = "scheduleItems";

        // integer, primary key auto_increment
        static final String KEY_ID = "id";

        // integer
        static final String KEY_GROUP_ID = "groupId";

        // integer
        static final String KEY_DISCIPLINE_ID = "disciplineId";

        // integer
        static final String KEY_DAY = "day";

        // integer
        static final String KEY_NUMBER_IN_DAY = "numberInDay";

        // text
        static final String KEY_AUDITORY = "auditory";

        // integer
        static final String KEY_PERIOD = "period";

        // integer
        static final String KEY_TEACHER_ID = "teacherId";
    }

    @Override
    public void putGroups(Collection<Group> groups) throws SQLException{
        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement()) {

            for(Group group : groups){
                statement.execute("INSERT INTO " + Groups.TABLE_NAME
                        + " (" + Groups.KEY_NAME + ")"
                        + " VALUES ('" + group.getName() + "')");
            }
        }
    }



    @Override
    public void putScheduleItems(Collection<ScheduleItem> scheduleItems) throws SQLException{
        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement()){
            for(ScheduleItem scheduleItem : scheduleItems){
                // combine period
                int period = 0;
                if (scheduleItem.repeatsEveryFirstWeek()){
                    period += 1 << 3;
                }
                if (scheduleItem.repeatsEverySecondWeek()){
                    period += 1 << 2;
                }
                if (scheduleItem.repeatsEveryThirdWeek()){
                    period += 1 << 1;
                }
                if (scheduleItem.repeatsEveryFourthWeek()){
                    period += 1;
                }

                String sql = "INSERT INTO " + ScheduleItems.TABLE_NAME
                        + " (" + ScheduleItems.KEY_GROUP_ID
                        + ", " + ScheduleItems.KEY_DISCIPLINE_ID
                        + ", " + ScheduleItems.KEY_DAY
                        + ", " + ScheduleItems.KEY_NUMBER_IN_DAY
                        + ", " + ScheduleItems.KEY_AUDITORY
                        + ", " + ScheduleItems.KEY_PERIOD
                        + ", " + ScheduleItems.KEY_TEACHER_ID + ") "
                        + " VALUES ('" + getIdByGroup(scheduleItem.getGroup()) +  "', '"
                        + getDisciplineId(scheduleItem.getDiscipline()) +  "', '"
                        + scheduleItem.getDay() +  "', '"
                        + scheduleItem.getNumberInDay() +  "', '"
                        + scheduleItem.getAuditory() +  "', '"
                        + period + "', '"
                        + getTeacherId(scheduleItem.getTeacher()) + "')";

                statement.execute(sql);
            }

        }
    }

    @Override
    public void putTeachers(Collection<Teacher> teachers) throws SQLException {
        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement()) {

            // add all missing teachers
            for (Teacher teacher : teachers) {
                String selectTeacher
                        = "SELECT * FROM " + Teachers.TABLE_NAME + " WHERE " + Teachers.KEY_FULL_NAME
                        + " = '" + teacher.getFullName() + "' AND " + Teachers.KEY_SHORT_NAME + " = '"
                        + teacher.getShortName() + "'";
                boolean rowExists;
                try (ResultSet rs = statement.executeQuery(selectTeacher)) {
                    rowExists = rs.next();
                }
                if (!rowExists) {
                    // add row
                    String sql = "INSERT INTO " + Teachers.TABLE_NAME
                            + " ("
                            + Teachers.KEY_SHORT_NAME
                            + "," + Teachers.KEY_FULL_NAME + ") "
                            + " VALUES ('" + teacher.getShortName() + "', '"
                            + teacher.getFullName() + "')";
                    statement.execute(sql);
                }
            }
        }
    }

    @Override
    public void putDisciplines(Collection<Discipline> disciplines) throws SQLException {
        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement()) {

            // add all missing disciplines
            for (Discipline discipline : disciplines) {
                String selectDiscipline
                        = "SELECT * FROM " + Disciplines.TABLE_NAME + " WHERE " + Disciplines.KEY_NAME
                        + " = '" + discipline.getName() + "'";
                boolean rowExists;
                try (ResultSet rs = statement.executeQuery(selectDiscipline)) {
                    rowExists = rs.next();
                }
                if (!rowExists) {
                    // add row
                    String sql = "INSERT INTO " + Disciplines.TABLE_NAME
                            + " (" + Disciplines.KEY_NAME + ")"
                            + " VALUES ('" + discipline.getName() + "')";

                    statement.execute(sql);
                }
            }
        }
    }

    private int getIdByGroup(Group group) throws SQLException {
        String sql = "SELECT * FROM " + Groups.TABLE_NAME + " WHERE " + Groups.KEY_NAME + " = '" + group.getName() + "'";

        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {

            rs.next();
            return rs.getInt(Groups.KEY_ID);
        }
    }

    private int getDisciplineId(Discipline discipline) throws SQLException {
        String sql = "SELECT * FROM " + Disciplines.TABLE_NAME + " WHERE " + Disciplines.KEY_NAME + " = '" + discipline.getName() + "'";

        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {

            rs.next();
            return rs.getInt(Disciplines.KEY_ID);
        }
    }

    private int getTeacherId(Teacher teacher) throws SQLException {
        String sql = "SELECT * FROM " + Teachers.TABLE_NAME + " WHERE " + Teachers.KEY_FULL_NAME + " = '" + teacher.getFullName() + "'";

        try (Connection conn = DriverManager.getConnection(jdbcDbUrl, username, password);
             Statement statement = conn.createStatement();
             ResultSet rs = statement.executeQuery(sql)) {

            rs.next();
            return rs.getInt(Teachers.KEY_ID);
        }
    }
}
