package com.mietrasp.data;

import com.mietrasp.model.Discipline;
import com.mietrasp.model.Group;
import com.mietrasp.model.ScheduleItem;
import com.mietrasp.model.Teacher;

import java.sql.SQLException;
import java.util.Collection;

/**
 * An interface for Schedule Database
 */
public interface ScheduleDb {

    /**
     * Puts a collection of {@link Group} to the database
     * @param groups to put to
     * @throws SQLException if any sql statement fails
     */
    void putGroups(Collection<Group> groups) throws SQLException;

    /**
     * Puts a collection of {@link Teacher} to the database
     * @param teachers to put to
     * @throws SQLException if any sql statement fails
     */
    void putTeachers(Collection<Teacher> teachers) throws SQLException;

    /**
     * Puts a collection of {@link Discipline} to the database
     * @param disciplines to put to
     * @throws SQLException if any sql statement fails
     */
    void putDisciplines(Collection<Discipline> disciplines) throws SQLException;

    /**
     * Puts a collection of {@link ScheduleItem} to the database
     * @param scheduleItems to put to
     * @throws SQLException if any sql statement fails
     */
    void putScheduleItems(Collection<ScheduleItem> scheduleItems) throws SQLException;

}
