package com.mietrasp;

import com.mietrasp.api.ScheduleApi;
import com.mietrasp.api.ScheduleApiImpl;
import com.mietrasp.api.ScheduleResponse;
import com.mietrasp.data.ScheduleDb;
import com.mietrasp.data.ScheduleDbImpl;
import com.mietrasp.model.Group;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Main {
    /**
     * Properties filename
     */
    private static final String CONFIG_FILENAME = "config.properties";
    /**
     * Properties key for database name
     */
    private static final String PROP_DB_NAME = "databaseName";
    /**
     * Properties key for db username
     */
    private static final String PROP_USERNAME = "username";
    /**
     * Properties key for db password
     */
    private static final String PROP_PASSWORD = "password";
    /**
     * Properties key for delay between requests to the server
     */
    private static final String PROP_DELAY = "delaySeconds";

    public static void main(String[] args) {
        String databaseName;
        String username;
        String password;

        int delaySeconds;
        try (InputStream in = new FileInputStream("config.properties")){
            Properties properties = new Properties();
            properties.load(in);
            databaseName = Objects.requireNonNull
                    (properties.getProperty(PROP_DB_NAME),
                            PROP_DB_NAME + " parameter in " + CONFIG_FILENAME + " must be set");

            username = Objects.requireNonNull
                    (properties.getProperty(PROP_USERNAME),
                            PROP_USERNAME + " parameter in " + CONFIG_FILENAME + " must be set");
            password = Objects.requireNonNull
                    (properties.getProperty(PROP_PASSWORD),
                            PROP_PASSWORD + " parameter in " + CONFIG_FILENAME + " must be set");

            delaySeconds = Integer.parseInt(Objects.requireNonNull
                    (properties.getProperty(PROP_DELAY),
                            PROP_DELAY + " parameter in " + CONFIG_FILENAME + " must be set"));

        } catch (FileNotFoundException e){
            System.err.println(CONFIG_FILENAME + "file was not found.");
            System.err.println("exiting");
            return;
        } catch (IOException e){
            System.err.println("Unable to read " + CONFIG_FILENAME);
            System.err.println("exiting");
            return;
        }

        ScheduleDb scheduleDb = new ScheduleDbImpl(databaseName, username, password);
        ScheduleApi scheduleApi = new ScheduleApiImpl();
        try {
            Set<Group> groups = scheduleApi.getGroups();
            System.out.println("Successfully received groups");
            System.out.println("Total groups: " + groups.size());
            scheduleDb.putGroups(groups);
            System.out.println("Successfully put groups to database");

            int groupsLeft = groups.size();

            for (Group group : groups){
                System.out.println("Going to get schedule for group: " + group.getName());
                ScheduleResponse scheduleResponse = scheduleApi.getScheduleByGroup(group);
                System.out.println("Successfully received schedule for group: " + group.getName());

                scheduleDb.putDisciplines(scheduleResponse.getDisciplines());
                scheduleDb.putTeachers(scheduleResponse.getTeachers());
                scheduleDb.putScheduleItems(scheduleResponse.getScheduleItems());
                System.out.println("Successfully put schedule for group " + group.getName() + " to the database.");
                System.out.println("Groups left: " + --groupsLeft);


                // delay to prevent too often sending requests to the server
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(delaySeconds));
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }


        } catch (IOException e){
            System.err.println("Unable to connect.");
            e.printStackTrace();
            return;
        } catch (SQLException e){
            System.err.println("SQL query failed...");
            e.printStackTrace();
            return;
        }


    }


}
