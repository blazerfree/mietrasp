function DayRasp (options) {
	var this_date = this.raspDate;

	options = options || {};
	options.intoId = options.intoId || "";
	options.nextBtnId = options.nextBtnId || options.intoId + "_next";
	options.prevBtnId = options.prevBtnId || options.intoId + "_prev";

	var lessons = JSON.parse(options.lessonsJson);
	var dateText = $("#"+options.dateTextId);
	var resultHtml = $("#"+options.intoId);
	var dateNow = moment();

	function init () {
		this_date = dateNow;
		renderDayLessons(dateNow);
	};

	function updateTechBlocks () {
		dateText.html('' + 
			this_date.format("DD MMM") +
			" (" + this_date.format("ddd") + ")"
		);
		resultHtml.data("date", dateNow);

	};

	function renderDayLessons (date) {
		var dayLessons = lessons[date.format("YYYY-MM-DD")];

		if (dayLessons) {
			var result = options.template({
				lessons: dayLessons
			});
			
			resultHtml.html(result);
		} else {
			resultHtml.html("<h3>ERR! Нет расписания данной группы на " +
				date.format("DD.MM.YY") +
				" (" + date.format("dddd") + ")" +
				"</h3>");
		}

		updateTechBlocks();
	}

	$("#"+options.prevBtnId).click(function(event) {
		event.preventDefault();

		this_date = moment(this_date).add(-1, 'days');
		renderDayLessons(this_date);
	});

	$("#"+options.nextBtnId).click(function(event) {
		event.preventDefault();

		this_date = moment(this_date).add(1, 'days');
		renderDayLessons(this_date);
	});

	init();
}