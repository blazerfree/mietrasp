var currentTime;
var allrasps = [];

function msg (msg) {
	UIkit.notify({
		message: msg,
		timeout: 5000,
		status: 'info',
	});
}

$.fn.rasp = function (options) {
	this.hasPassed = false; // Прошло ли событие
	this.progress = 0;

	options = options || {};
	options.beginTime = options.beginTime;
	options.endTime = options.endTime;

	var $this = $(this);
	var date = $this.data('date');
	var beginTime = moment(date + ' ' + options.beginTime, 'DD.MM.YYYY HH:mm');
	var endTime = moment(date + ' ' + options.endTime, 'DD.MM.YYYY HH:mm');
	var diffTimes = endTime.diff(beginTime);

	var progressBar = $this.find('.i-rasp-progress');
	var progressBarBackground = '#faa732';

	this.update = function () {
		if (!this.hasPassed) {
			this.progress = 1 * moment().diff(beginTime) / diffTimes;

			if (this.progress >= 1) {
				progressBarBackground = '#82bb42';
				this.hasPassed = true;
				this.progress = 1;
				$this.addClass('m-passed')
			}

			progressBar.css({
				height: this.progress * 100 + '%',
				background: progressBarBackground
			});
		}
	}
	return this;
}


function updateTime () {
   	currentTime = moment();
   	allrasps.forEach(function (item) {
   		item.update();
   	});
    // datetime.html();
    // msg(currentTime.format('dddd'));
};

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}

var oldGroup;
function chengeGroup() {
	var group = $('#group');

	var VRegExp = new RegExp(/([А-Я]+)-([1-4]{1}[0-9]{1})$/);
	var VResult = VRegExp.test(group.val());

	if(!VResult){
		group.val('Неверная группа');
		group.css('border-color','red');
		return ;
	}



	if(oldGroup != group.val()) {

		var date = getUrlVars()['date'] != undefined ? '&date='+getUrlVars()["date"] : '';
		var type = getUrlVars()['type'] != undefined ? '&type='+getUrlVars()["type"] : '';

		var cookDate = new Date(new Date().getTime() + 10368000 * 1000);
		document.cookie = 'group=' + group.val() + '; path=/; expires=' + cookDate.toUTCString();
		document.location.href = '/index.php?r=rasp/show-rasp&group=' + group.val()+date+type;
	}
}

var oldTeacher;
function chengeTeacher() {
	var teacher = $('#teacher');

	var VRegExp = new RegExp(/[^А-Яа-яеёЁЕ\s]/);
	var VResult = VRegExp.test(teacher.val());
	if(VResult){
		teacher.val('Неверный преподаватель');
		teacher.css('border-color','red');
		return ;
	}

	if(oldTeacher != teacher.val()) {

		var date = getUrlVars()['date'] != undefined ? '&date='+getUrlVars()["date"] : '';
		var type = getUrlVars()['type'] != undefined ? '&type='+getUrlVars()["type"] : '';

		var cookDate = new Date(new Date().getTime() + 10368000 * 1000);
		document.cookie = 'teacher=' + teacher.val() + '; path=/; expires=' + cookDate.toUTCString();
		document.location.href = '/index.php?r=rasp/show-rasp&teacherName=' + teacher.val()+date+type;
	}
}

function findTeacher() {
	var week = $('#findWeek').val();
	var day = $('#findDay').val();
	var pare = $('#findP').val();
	var teacher = $('#teacher').val();
	$('#findWeek').css('border','');
	$('#findP').css('border','');

	$.ajax({
		type: 'GET',
		url: '/index.php',
		data: 'r=rasp/where-teacher&week='+week+'&day='+day+'&pare='+pare+'&teacherName='+teacher,
		success: function(data){
			if(data == "error"){
				$('#findWeek').css('border','solid 1px red');
				$('#findP').css('border','solid 1px red');

			}else {
				if (data != "")
					$('#whereTeacher').text(data);
				else
					$('#whereTeacher').text("Не известно");
			}
		}
	});



}


$(document).ready(function() {
	moment.locale('ru');

	$('.i-rasps .i-rasp').each(function() {
		var rasp = $(this);

		var thisRasp = rasp.rasp({
			beginTime: rasp.find('.i-rasp-beginTime').text(),
			endTime: rasp.find('.i-rasp-endTime').text(),
		});
		
		allrasps.push(thisRasp);	
	});


	updateTime();
	setInterval(updateTime, 1000);

	//новые сообщения
	if($('#isUser') && $('#isStudent')) {
		$.get("/index.php?r=teacher/new-messages",function (res) {
			if(res!=0){
				$('#countMesseges').css('display','inline').append(res);
			}
		});
	}

	//изменение группы на главной
	$('#group').focus(function (event) {
		var group = $('#group');
		if(group.val() == "Неверная группа") {
			group.val("");
			group.css("border-color","#dddddd");
		}
		oldGroup = group.val();
	});
	$('#group').blur(chengeGroup);

	//изменение группы на главной
	$('#teacher').focus(function (event) {
		var teacher = $('#teacher');
		if(teacher.val() == "Неверный преподаватель") {
			teacher.val("");
			teacher.css("border-color","#dddddd");
		}
		oldTeacher = teacher.val();
	});
	$('#teacher').blur(chengeTeacher);


	$('#dayrasp_next').on('click',function () {
		var oldDate = Number($('#this_date').val());
		var newDate = oldDate + 86400;
		var group = $('#group');
		document.location.href = '/index.php?r=rasp/show-rasp&group='+group.val()+'&date='+newDate;
	})
	$('#dayrasp_prev').on('click',function () {
		var oldDate = Number($('#this_date').val());
		var newDate = oldDate - 86400;
		var group = $('#group');
		document.location.href = '/index.php?r=rasp/show-rasp&group='+group.val()+'&date='+newDate;
	})

	$('#next_week').on('click',function () {
		if($('#this_week').val()!=16) {
			var oldDate = Number($('#this_week_date').val());
			var newDate = oldDate + 7 * 24 * 60 * 60;
			var group = $('#group');
			document.location.href = '/index.php?r=rasp/show-rasp&group=' + group.val() + '&type=week&date=' + newDate;
		}
	})
	$('#prev_week').on('click',function () {
		if($('#this_week').val()!=1) {
			var oldDate = Number($('#this_week_date').val());
			var newDate = oldDate - 7 * 24 * 60 * 60;
			var group = $('#group');
			document.location.href = '/index.php?r=rasp/show-rasp&group=' + group.val() + '&type=week&date=' + newDate;
		}
	})

	$('#yesterday').on('click',function () {
		var group = $('#group');
		var newDate = parseInt(new Date().getTime()/1000-86400);
		document.location.href = '/index.php?r=rasp/show-rasp&group='+group.val()+'&date='+newDate;
	});
	$('#today').on('click',function () {
		var group = $('#group');
		var newDate = parseInt(new Date().getTime()/1000);
		document.location.href = '/index.php?r=rasp/show-rasp&group='+group.val()+'&date='+newDate;
	});
	$('#tomorrow').on('click',function () {
		var group = $('#group');
		var newDate = parseInt(new Date().getTime()/1000+86400);
		document.location.href = '/index.php?r=rasp/show-rasp&group='+group.val()+'&date='+newDate;
	});
	//$('.i-select').select2();
});

function showNotes(schName,notes,schId) {
	$('#notes_list').empty();
	$('#schName').text(schName);
	$('#disciplineId').val(schId);
	if(notes != null) {
		notes.forEach(function (note, i, notes) {
			var date = new Date(note['created_at'] * 1000);
			$('#notes_list').append('<div id="note_' + note["id"] + '" class="b-note uk-panel uk-panel-box uk-panel-box-secondary">' +
				'<div class="b-note-delete">' +
				'<a href="#" class="uk-button uk-button-mini uk-button-danger" onclick="deleteNote(' + note["id"] + ')"><i class="uk-icon-close"></i></a>' +
				'</div>' +
				'<div class="b-note-date uk-badge uk-badge-info">' +
				date.getDate() + "." + date.getMonth() + "." + date.getFullYear() +
				'</div>' +
				'<div class="b-note-text">' +
				note['text'] +
				'</div>' +
				'</div><!-- .b-note -->')
		});
	}
}

function deleteNote(id) {
	$('#note_'+id).remove();
	$.get( "/index.php?r=notes/delete-note&id="+id);
}


function setEmptyTd(id) {
	$('#'+id).addClass('m-empty');
}

