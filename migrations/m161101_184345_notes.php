<?php

use yii\db\Migration;

class m161101_184345_notes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('notes', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'subject_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
            'complete' => $this->boolean(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        //$this->insert('user',['id'=>1,'name'=>'Алексей','password_hash'=>'1212','email'=>'l.novicovdean@mail.ru','student'=>true,'created_at'=>1212]);
    }
    public function down()
    {
        $this->dropTable('notes');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
