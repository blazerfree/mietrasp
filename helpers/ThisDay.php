<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.11.2016
 * Time: 12:06
 */

namespace app\helpers;


use app\models\Group;
use app\models\Teacher;

class ThisDay
{
    public static function getThisWeek($date = false)
    {
        if (!$date)
            $date = time();

        $thisWeek = date('W', $date);
        if ($thisWeek >= 35 && $thisWeek <= 53)
            $thisWeek -= 34;
        else
            $thisWeek -= 5;

        $type = '';
        switch ($thisWeek % 4) {
            case 1:
                $type = 'ч-I';
                break;
            case 2:
                $type = 'з-I';
                break;
            case 3:
                $type = 'ч-II';
                break;
            case 0:
                $type = 'з-II';
                break;
        }

        return ['week' => $thisWeek, 'type' => $type];
    }

    public static function getGroupsName(){
        return Group::find()->distinct()->select('name')->asArray()->all();
    }

    public static function getTeachersName(){
        return Teacher::find()->distinct()->select('fullName')->asArray()->all();
    }
}